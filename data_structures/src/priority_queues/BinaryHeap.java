package priority_queues;

import java.util.ArrayList;
import java.util.Collections;

public class BinaryHeap<T extends Comparable<T>> {
	private ArrayList<T> heap;

	public BinaryHeap() {
		heap = new ArrayList<T>();
	}

	public BinaryHeap(T[] elements) {
		heap = new ArrayList<T>();
		for (T element : elements) {
			heap.add(element);
		}
		for (int i = heap.size()/2; i>-1; i--) {
			if (!isALeaf(i)) {
				filterDown(i);
			}

		}
	}

	/**
	 * Indicates whether the binary heap is empty or not.
	 * 
	 * @return true if the binary heap is empty, false otherwise.
	 */
	public boolean isEmpty() {
		return heap.isEmpty();
	}

	public void print() {
		System.out.println(toString());
	}

	public String toString() {
		return heap.toString();
	}

	/**
	 * Starting from the given position, checks whether the element is lower than
	 * its father, swapping their positions if it is.
	 * 
	 * @param pos
	 *            to start the filtering
	 */
	private void filterUp(int pos) {
		for (int i = heap.size() - 1; i > 0; i--) {
			if (heap.get(i).compareTo(heap.get((i - 1) / 2)) < 0) {
				Collections.swap(heap, i, ((i - 1) / 2));
			}
		}
	}

	/**
	 * Starting from the given position, checks whether the element is lower than
	 * one of their children, swapping their positions if it is.
	 * 
	 * @param pos
	 *            to start the filtering
	 */
	private void filterDown(int pos) {
		while (!isALeaf(pos)) {
			T aux = null;

			if (((2 * pos + 2 < heap.size()) && (2 * pos + 1 < heap.size()))
					&& (heap.get(2 * pos + 2).compareTo(heap.get(2 * pos + 1)) < 0)) {
				aux = heap.get(2 * pos + 2);
			} else {
				aux = heap.get(2 * pos + 1);
			}

			if (aux != null && heap.get(pos).compareTo(aux) > 0) {
				Collections.swap(heap, pos, heap.indexOf(aux));
			}
			pos++;
		}
	}

	/**
	 * Checks whether the element at the given position is a leaf or not.
	 * 
	 * @param pos
	 *            to be checked
	 * @return whthter the element at pos is a leaf or not
	 */
	private boolean isALeaf(int pos) { // everything from the middle of the array to the end is a leaf
		return (pos >= heap.size() / 2);
	}

	/**
	 * Adds a new element to the queue.
	 * 
	 * @param element
	 *            to be added
	 */
	public void add(T element) {
		heap.add(element);
		filterUp(heap.size() - 1);
	}

	/**
	 * Allows to access the minimum element in the queue, which will be then removed.
	 * 
	 * @return the minimum stored element
	 */
	public T getMin() {
		T result = null;
		if (!heap.isEmpty()) {

			result = heap.get(0);
			Collections.swap(heap, 0, heap.size() - 1);
			heap.remove(heap.size() - 1);
			filterDown(0);
		}
		return result;
	}
}
