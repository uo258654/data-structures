package priority_queues;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestPriorityQueues {

	@Test
	public void testAdd() {
		BinaryHeap<Integer> a = new BinaryHeap<Integer>();
		a.add(10);
		a.add(9);
		a.add(8);
		assertEquals(a.toString(), ("[8, 10, 9]"));
		a.add(7);
		assertEquals(a.toString(), ("[7, 8, 9, 10]"));
		a.add(6);
		assertEquals(a.toString(), ("[6, 7, 9, 10, 8]"));
		a.add(5);
		assertEquals(a.toString(), ("[5, 7, 6, 10, 8, 9]"));
		a.add(4);
		assertEquals(a.toString(), ("[4, 7, 5, 10, 8, 9, 6]"));
	}

	@Test
	public void testGetMin() {
		BinaryHeap<Integer> a = new BinaryHeap<Integer>();
		a.add(9);
		a.add(8);
		a.add(7);
		a.add(6);
		a.add(5);
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);

		assertEquals("[1, 3, 2, 4, 7, 8, 5, 9, 6]", a.toString());
		try {
			assertEquals(1, (int) a.getMin());
		} catch (Exception e) {
			fail("An esception has been catched");
		}
		
		assertEquals("[2, 3, 5, 4, 7, 8, 6, 9]", a.toString());
	}

}
