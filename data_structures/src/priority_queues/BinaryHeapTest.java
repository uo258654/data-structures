package priority_queues;

import static org.junit.Assert.*;

import org.junit.Test;

public class BinaryHeapTest {

	@Test
	public void test_sort_order_A() {

		Integer[] input = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(input);

		String result = "";

		// print elements in sorted order
		while (!heap.isEmpty()) {
			int x;
			try {
				x = heap.getMin();
				result += x;
			} catch (Exception e) {
				fail("An exceptions has been raised.");
			}

		}

		assertEquals("1234567891011", result);
	}

	@Test
	public void test_sort_order_B() {

		BinaryHeap<Integer> heap = new BinaryHeap<Integer>();
		heap.add(2);
		heap.add(5);
		heap.add(1);
		heap.add(9);

		String result = "";

		// print elements in sorted order
		while (!heap.isEmpty()) {
			int x;
			try {
				x = heap.getMin();
				result += x;
			} catch (Exception e) {
				fail("An exceptions has been raised.");
			}

		}

		assertEquals(result, "1259");
	}

}