package hash_table;

import static org.junit.Assert.*;

import org.junit.Test;


public class HashTableTest {

	
	
	@Test
	public void testPrimeNumber()
	{
		HashTable<Integer> a;
		
		// Example
		try {
			a = new HashTable<Integer>(5, HashTable.LINEAR_PROBING, 1.0);
			
			assertEquals(2, a.getNextPrimeNumber(1));
			assertEquals(3, a.getNextPrimeNumber(2));
			assertEquals(5, a.getNextPrimeNumber(3));
			assertEquals(5, a.getNextPrimeNumber(4));
			assertEquals(7, a.getNextPrimeNumber(5));
			assertEquals(7, a.getNextPrimeNumber(6));
			assertEquals(11, a.getNextPrimeNumber(7));
			assertEquals(11, a.getNextPrimeNumber(8));
			assertEquals(11, a.getNextPrimeNumber(9));
			assertEquals(11, a.getNextPrimeNumber(10));
			assertEquals(13, a.getNextPrimeNumber(11));
			
			assertEquals(13, a.getPrevPrimeNumber(15));
			assertEquals(13, a.getPrevPrimeNumber(14));
			assertEquals(11, a.getPrevPrimeNumber(13));
			assertEquals(11, a.getPrevPrimeNumber(12));
			assertEquals(7, a.getPrevPrimeNumber(11));
			assertEquals(7, a.getPrevPrimeNumber(10));
			assertEquals(7, a.getPrevPrimeNumber(9));
			assertEquals(7, a.getPrevPrimeNumber(8));
			assertEquals(5, a.getPrevPrimeNumber(7));
			assertEquals(5, a.getPrevPrimeNumber(6));
			assertEquals(3, a.getPrevPrimeNumber(5));
			assertEquals(3, a.getPrevPrimeNumber(4));
			assertEquals(2, a.getPrevPrimeNumber(3));
		}
		catch (Exception e)
		{
			System.out.println(e);
			fail();
		}
	}
	
	
	@Test
	public void test_f()
	{
		HashTable<Integer> a;
		HashTable<Character> b;
		
		try {
			a = new HashTable<Integer>(7, HashTable.LINEAR_PROBING, 1.0);
			assertEquals(6, a.f(6, 0));
			assertEquals(0, a.f(6, 1));
			assertEquals(1, a.f(6, 2));
			assertEquals(2, a.f(6, 3));
		}
		catch (Exception e)
		{
			System.out.println(e);
			fail();
		}
		
		try {
			b = new HashTable<Character>(7, HashTable.QUADRATIC_PROBING, 1.0);
			assertEquals(1, b.f('c', 0));
			assertEquals(2, b.f('c', 1));
			assertEquals(5, b.f('c', 2));
			assertEquals(3, b.f('c', 3));
		}
		catch (Exception e)
		{
			System.out.println(e);
			fail();
		}
	}
	
	
	@Test
	public void test_table_linear()
	{
		HashTable<Integer> a;
		
		try {
			a = new HashTable<Integer>(7, HashTable.LINEAR_PROBING, 1.0);
			a.add(34);
			a.add(6);
			a.add(13);
			assertEquals("[0] (1) = 6 - [1] (1) = 13 - [2] (0) = null - [3] (0) = null - [4] (0) = null - [5] (0) = null - [6] (1) = 34 - ", a.toString());
			
			a.remove(6);
			assertEquals(false, a.search(6));
			assertEquals(true, a.search(13));
			assertEquals("[0] (2) = 6 - [1] (1) = 13 - [2] (0) = null - [3] (0) = null - [4] (0) = null - [5] (0) = null - [6] (1) = 34 - ", a.toString());
		}
		catch (Exception e)
		{
			System.out.println(e);
			fail();
		}
	}
	
	
	@Test
	public void test_table_cuadratic()
	{
		HashTable<Character> a;
		
		try {
			a = new HashTable<Character>(7, HashTable.QUADRATIC_PROBING, 1.0);
			a.add('b');
			a.add('c');
			a.add('j');
			a.add('q');
			assertEquals("[0] (1) = b - [1] (1) = c - [2] (1) = j - [3] (0) = null - [4] (0) = null - [5] (1) = q - [6] (0) = null - ", a.toString());
			
			a.remove('j');
			assertEquals(false, a.search('j'));
			assertEquals(true, a.search('q'));
			assertEquals("[0] (1) = b - [1] (1) = c - [2] (2) = j - [3] (0) = null - [4] (0) = null - [5] (1) = q - [6] (0) = null - ", a.toString());
			
			a.add('x');
			assertEquals(true, a.search('x'));
			assertEquals(true, a.search('q'));
			assertEquals("[0] (1) = b - [1] (1) = c - [2] (1) = x - [3] (0) = null - [4] (0) = null - [5] (1) = q - [6] (0) = null - ", a.toString());
		}
		catch (Exception e)
		{
			System.out.println(e);
			fail();
		}
	}
		

	

}