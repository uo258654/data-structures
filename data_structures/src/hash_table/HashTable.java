package hash_table;

import java.util.ArrayList;
import java.util.List;

public class HashTable<T> {

	protected final static int LINEAR_PROBING = 0;
	protected final static int QUADRATIC_PROBING = 1;
	protected final static int DOUBLE_HASHING = 2; // [hash + i*(R-x%B)]%B

	private int B = 7; // maximun number of lements
	private int redispersionType = LINEAR_PROBING;
	private double minLF = 0.5;
	private double n; // actual number of elements in the hashtable
	private ArrayList<HashNode<T>> associativeArray;
	private int R = 5;

	public HashTable(int B, int redispersionType, double minLF) {
		setB(B);
		this.R = getPrevPrimeNumber(B);
		n = 0;
		this.associativeArray = initializeAssociativeArray(B);
		setRedispersionType(redispersionType);
		setMinLF(minLF);
	}

	private ArrayList<HashNode<T>> initializeAssociativeArray(int B) {
		ArrayList<HashNode<T>> associativeArray = new ArrayList<HashNode<T>>(B);
		for (int i = 0; i < B; i++) {
			associativeArray.add(new HashNode<T>());
		}
		return associativeArray;

	}

	public ArrayList<HashNode<T>> getAssociativeArray() {
		return associativeArray;
	}

	public int getB() {
		return B;
	}

	public void setB(int B) {
		this.B = B;
	}

	public int getRedispersionType() {
		return redispersionType;
	}

	public void setRedispersionType(int redispersionType) {
		this.redispersionType = redispersionType;
	}

	public double getMinLF() {
		return minLF;
	}

	public void setMinLF(double minLF) {
		this.minLF = minLF;
	}

	/**
	 * Hashing function.
	 * 
	 * @param element
	 *            to be stored
	 * @param i
	 *            Attempt number
	 * @return slot in the array where the element should be placed
	 */
	protected int f(T element, int i) {
		int h = element.hashCode();
		int result = 0;
		switch (redispersionType) {
		case LINEAR_PROBING:
			result = (h + i) % getB();
			break;
		case QUADRATIC_PROBING:
			result = (h + (int) (Math.pow(i, 2))) % getB();
			break;
		case DOUBLE_HASHING:
			result = (h + i * h2(h)) % getB();
		}
		return result;
	}

	private int h2(int x) {

		return R - (x % R);
	}

	public void add(T element) {
		if (element != null) {
			int n = 0;
			int pos = f(element, n);
			while (pos < associativeArray.size()) {
				if (associativeArray.get(pos).getStatus() == HashNode.EMPTY
						|| associativeArray.get(pos).getStatus() == HashNode.DELETED) {
					associativeArray.get(pos).setElement(element);
					associativeArray.get(pos).setStatus(HashNode.VALID);
					this.n++;
					break;
				}
				n++;
				pos = f(element, n);
			}
			
			if(getLF() > getMinLF()) {
				dynamicResize();
			}
		}

	}

	public boolean search(T element) {
		boolean result = false;
		if (element != null) {
			int n = 0;
			int pos = f(element, n);
			do {
				if (element.equals(associativeArray.get(pos).getElement())) {
					result = !(associativeArray.get(pos).getStatus() == HashNode.DELETED);
					break;
				}
				n++;
				pos = f(element, n);

			} while (n < associativeArray.size());

		}
		return result;
	}

	public void remove(T element) {
		if (element != null) {
			int n = 0;
			int pos = f(element, n);
			do {
				if (element.equals(associativeArray.get(pos).getElement())) {
					associativeArray.get(pos).setStatus(HashNode.DELETED);
					this.n--;
					break;
				}
				n++;
				pos = f(element, n);

			} while (n < associativeArray.size());

		}
	}

	public String toString() {
		String result = "";
		for (int i = 0; i < associativeArray.size(); i++) {
			T element = associativeArray.get(i).getElement();
			String status = String.valueOf(associativeArray.get(i).getStatus());
			String elementString = "null";
			if (element != null) {
				elementString = element.toString();
			}
			result += ("[" + i + "]" + " (" + status + ") = " + elementString.toString() + " - ");
		}

		return result;
	}

	public void print() {
		System.out.println(toString());
	}

	public double getLF() {
		return n / B;
	}

	private boolean isPrime(int n) {
		if (n == 2) {
			return true;
		}
		if (n % 2 == 0) {
			return false;
		}
		for (int i = 3; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}

		return true;
	}

	int getPrevPrimeNumber(int n) {
		int result = 1;
		for (int i = n - 1; i > 0; i--) {
			if (isPrime(i)) {
				result = i;
				break;
			}
		}
		return result;
	}

	int getNextPrimeNumber(int n) {
		int result = n;
		for (int i = n + 1; i < Integer.MAX_VALUE; i++) {
			if (isPrime(i)) {
				result = i;
				break;
			}
		}
		return result;
	}

	public void dynamicResize(int newSize) {
		ArrayList<HashNode<T>> oldArray = getAssociativeArray();
		setB(newSize);
		n = 0;
		associativeArray = initializeAssociativeArray(newSize);
		for (HashNode<T> node : oldArray) {
			if (node.getStatus() == HashNode.VALID) {
				add(node.getElement());
			}
		}
	}

	public void dynamicResize() {
		dynamicResize(getNextPrimeNumber(B * 2));
	}
	
	

}
