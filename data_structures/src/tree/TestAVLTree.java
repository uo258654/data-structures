package tree;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestAVLTree {


	@Test
	public void testAddDoubleRotation() {
		
	AVLTree<Character> a = new AVLTree<Character>();
	a.add('e');
	a.add('g');
	a.add('b');
	a.add('d');
	a.add('c');
	
	assertEquals("e(-1)c(0)b(0)--d(0)--g(0)--", a.toString());
	}
	@Test
	public void testAddSingleLeftRotation() {
		AVLTree<Character> a = new AVLTree<Character>();

		a.add('a');
		a.add('b');
		a.add('c');
		a.add('d');
		a.add('e');

		assertEquals("b(1)a(0)--d(0)c(0)--e(1)-f(0)--", a.toString());

		a.add('f');
		assertEquals("d(0)b(0)a(0)--c(0)--e(1)-f(0)--", a.toString());
	}
}
