package tree;

public class AVLNode<T> {

	private T element;
	private AVLNode<T> left;
	private AVLNode<T> rigth;
	private int height;

	public AVLNode(T element) {
		setElement(element);
		setLeft(null);
		setRigth(null);
	}

	public AVLNode(T element, AVLNode<T> left, AVLNode<T> rigth) {
		setElement(element);
		setLeft(left);
		setRigth(rigth);
	}

	public T getElement() {
		return element;
	}

	protected void setElement(T element) {
		this.element = element;
	}

	/**
	 * Getter allowing to access the left branche's node.
	 * 
	 * @return the node accessed by the left branch.
	 */
	public AVLNode<T> getLeft() {
		return left;
	}

	/**
	 * Set the new content of the left branch
	 * 
	 * @param AVLNode<T>
	 *            the new node to be accessed by the left branch.
	 */
	protected void setLeft(AVLNode<T> left) {
		this.left = left;
	}

	/**
	 * Getter allowing to access the right branche's node.
	 * 
	 * @return the node accessed by the right branch.
	 */
	public AVLNode<T> getRigth() {
		return rigth;
	}

	/**
	 * Set the new content of the right branch
	 * 
	 * @param AVLNode<T>
	 *            the new node to be accessed by the right branch.
	 */
	protected void setRigth(AVLNode<T> rigth) {
		this.rigth = rigth;
	}

	public int getHeight() {
		return height;
	}

	protected void setHeight(int height) {
		this.height = height;
	}

	public String toString() {
		if (element != null)
			return getElement().toString() + "(" + getBF() + ")";
		else
			return "-";
	}

	public void print() {
		System.out.println(this.toString());
	}

	/**
	 * Updates the height property of the node, based on the height of the children
	 * nodes.
	 */
	protected void updateHeight() {
		if (left == null && rigth == null) {
			height = 0;
		} else {
			if (rigth == null) {
				height = 1 + left.height;
			} else {
				if (left == null) {
					height = 1 + rigth.height;
				} else {
					if (rigth.height > left.height) {
						height = 1 + rigth.height;
					} else {
						height = 1 + left.height;
					}
				}
			}
		}

	}

	/**
	 * Allows to obtain the balance factor for the current node.
	 * 
	 * @return int containing the balance factor of the node. If the tree is
	 *         balanced, it should be -1, 0 or 1.
	 */
	protected int getBF() {
		int leftHeigh = 0;
		int rigthHeigh = 0;
		if (getLeft() != null) {
			leftHeigh = 1 + getLeft().getHeight();
		}
		if (getRigth() != null) {
			rigthHeigh = 1 + getRigth().getHeight();
		}
		return rigthHeigh - leftHeigh;

	}

}
