package tree;

import java.io.IOException;
import java.io.StringWriter;

public class AVLTree<T extends Comparable<T>> {

	private AVLNode<T> root;

	public AVLTree(T element) {
		root = new AVLNode<T>(element);
	}

	public AVLTree() {
		root = new AVLNode<T>(null);
	}

	public AVLNode<T> getRoot() {
		return root;
	}

	public int getHeight() {
		int counter = 0;
		if (root != null) {
			counter++;
			if (root.getBF() == -1) {
				counter += getHeighRec(root.getLeft(), counter);
			} else if (root.getBF() == 1) {
				counter += getHeighRec(root.getRigth(), counter);
			} else {
				if (root.getLeft() != null) {
					counter += getHeighRec(root.getLeft(), counter);
				} else if (root.getRigth() != null) {
					counter += getHeighRec(root.getRigth(), counter);
				}
			}
		}

		return counter;
	}

	private int getHeighRec(AVLNode<T> theRoot, int counter) {

		if (theRoot.getBF() == -1) {
			counter += getHeighRec(theRoot.getLeft(), counter);
		} else if (theRoot.getBF() == 1) {
			counter += getHeighRec(theRoot.getRigth(), counter);
		} else {
			if (theRoot.getLeft() != null) {
				counter += getHeighRec(theRoot.getLeft(), counter);
			} else if (theRoot.getRigth() != null) {
				counter += getHeighRec(theRoot.getRigth(), counter);
			}
		}
		return counter++;
	}

	/**
	 * Adds a new element to the tree. May throw an IllegalArgumentException if null
	 * is given as a parameter.
	 * 
	 * @param element
	 *            to be inserted in the tree.
	 */
	public void add(T element) {
		if (element == null) {
			throw new IllegalArgumentException("Cannot insert null as an element.");
		}
		if (root.getElement() == null) {
			root = new AVLNode<T>(element);
		} else {
			root = addRec(element, root);
		}

	}

	/**
	 * Recursive auxiliary method to the normal add.
	 * 
	 * @param element
	 *            to be inserted in the tree.
	 * @param root2
	 *            the new node to be considered as root.
	 */
	private AVLNode<T> addRec(T element, AVLNode<T> root2) {
		if (element.compareTo(root2.getElement()) == 0) {
			throw new IllegalArgumentException("the element already exists");
		}

		if (element.compareTo(root2.getElement()) < 0) {
			 if (root2.getLeft() == null) {
			 root2.setLeft(new AVLNode<T>(element));
			 } else {
			root2.setLeft(addRec(element, root2.getLeft()));
			 }
		}

		if (element.compareTo(root2.getElement()) > 0) {
			 if (root2.getRigth() == null) {
			 root2.setRigth(new AVLNode<T>(element));
			 } else {
			root2.setRigth(addRec(element, root2.getRigth()));
			 }
		}
		return (updateBF(root2));
		 // needs to be changed to updateBF
	}

	/**
	 * Returns an String representation of the tree, in pre-order traversal.
	 */
	public String toString() {
		StringWriter path = new StringWriter();

		path.append(root.toString());

		if (root.getLeft() != null) {
			toStringRec(root.getLeft(), path);
		} else {
			path.append("-");
		}
		if (root.getRigth() != null) {
			toStringRec(root.getRigth(), path);
		} else {
			path.append("-");
		}
		String finalPath = path.toString();
		try {
			path.close();
		} catch (IOException e) {

		}

		return finalPath;
	}

	private StringWriter toStringRec(AVLNode<T> node, StringWriter path) {

		path.append(node.toString());
		if (node.getLeft() != null) {
			toStringRec(node.getLeft(), path);
		} else {
			path.append("-");
		}
		if (node.getRigth() != null) {
			toStringRec(node.getRigth(), path);
		} else {
			path.append("-");
		}

		return path;
	}

	/**
	 * Checks whether the current tree constains an element.
	 * 
	 * @param element
	 *            to be searched
	 * @return boolean true if the element is in the tree, and false otherwise.
	 */
	public boolean search(T element) {
		boolean elementExists = false;
		if (root != null) {
			{
				if (element.compareTo(root.getElement()) == 0) {
					elementExists = true;
				} else {
					if (element.compareTo(root.getElement()) < 0) {
						if (root.getLeft() != null)
							elementExists = searchRec(root.getLeft(), element);
					} else {
						if (element.compareTo(root.getElement()) > 0)
							if (root.getRigth() != null)
								elementExists = searchRec(root.getRigth(), element);
					}
				}
			}
		}

		return elementExists;
	}

	/**
	 * Recusrsive auxiliary method for the search.
	 * 
	 * @param node
	 *            to be considered as the new root.
	 * @param element
	 *            to be searched
	 * @return boolean true if the element is found, and false otherwise.
	 */
	private boolean searchRec(AVLNode<T> node, T element) {
		boolean elementExists = false;
		if (element.compareTo(node.getElement()) == 0) {
			elementExists = true;
		} else {
			if (element.compareTo(node.getElement()) < 0) {
				if (node.getLeft() != null)
					elementExists = searchRec(node.getLeft(), element);
			} else {
				if (element.compareTo(node.getElement()) > 0)
					if (node.getRigth() != null)
						elementExists = searchRec(node.getRigth(), element);
			}
		}
		return elementExists;
	}

	/**
	 * Returns the maximum element on the right branch of the given root.
	 * 
	 * @param theRoot
	 *            whose right branch would be analyzed.
	 * @return the maximum element of the right branch.
	 */
	protected T getMax(AVLNode<T> theRoot) {
		while (theRoot.getRigth() != null) {
			theRoot = theRoot.getRigth();
		}
		return theRoot.getElement();

	}

	/**
	 * Removes a given element from the tree.
	 * 
	 * @param element
	 *            to be removed
	 */
	public void remove(T element) {

		if (root == null) {
			throw new RuntimeException("The element does not exists.");
		} else {
			root = removeRec(root, element);
		}

	}

	/**
	 * Auxiliary recursive method for removal.
	 * 
	 * @param theRoot
	 *            to be considered.
	 * @param element
	 *            to be removed
	 * @return
	 */
	private AVLNode<T> removeRec(AVLNode<T> theRoot, T element) {

		if (element.compareTo(theRoot.getElement()) < 0) {
			theRoot.setLeft(removeRec(theRoot.getLeft(), element));
		} else {
			if (element.compareTo(theRoot.getElement()) > 0) {
				theRoot.setRigth(removeRec(theRoot.getRigth(), element));
			} else {
				if (theRoot.getLeft() == null && theRoot.getRigth() == null) {
					return null;
				} else {

					if (theRoot.getLeft() == null && theRoot.getRigth() != null) {
						return theRoot.getRigth();
					} else {

						if (theRoot.getLeft() != null && theRoot.getRigth() == null) {
							return theRoot.getLeft();
						} else {

							T newElement = getMax(theRoot.getLeft());
							theRoot.setElement(newElement);
							theRoot.setLeft(removeRec(theRoot.getLeft(), newElement));

						}
					}
				}
			}

		}
		return (updateBF(theRoot));
	}

	/**
	 * Given another tree, joins it with the current one.
	 * 
	 * @param tree
	 *            to be joined with the current one.
	 * @return the new tree joining the current one and the parameter.
	 */
	public AVLTree<T> joins(AVLTree<T> tree) {
		AVLTree<T> newTree = this;
		addInPreOrder(newTree, tree.getRoot());
		return newTree;
	}

	private void addInPreOrder(AVLTree<T> newTree, AVLNode<T> root) {
		try {
			newTree.add(root.getElement());
		} catch (IllegalArgumentException e) {

		}

		if (root.getLeft() != null) {
			try {
				addInPreOrder(newTree, root.getLeft());
			} catch (IllegalArgumentException e) {

			}
		}

		if (root.getRigth() != null) {
			try {
				addInPreOrder(newTree, root.getRigth());
			} catch (IllegalArgumentException e) {

			}
		}

	}

	/**
	 * Generates a new tree, which only contains the elements present in the current
	 * tree and the one given as a parameter.
	 * 
	 * @param tree
	 *            whose intersection with the current one is to be calculated .
	 * @return the tree containing the elements present in both trees.
	 */
	public AVLTree<T> intersection(AVLTree<T> tree) {

		AVLTree<T> newTree = new AVLTree<T>();

		intersectInPreOrder(tree, this.getRoot(), newTree);

		return newTree;
	}

	private void intersectInPreOrder(AVLTree<T> tree, AVLNode<T> root, AVLTree<T> newTree) {

		try {
			if (tree.search(root.getElement())) {
				newTree.add(root.getElement());
			}
		} catch (IllegalArgumentException e) {

		}

		if (root.getLeft() != null) {
			try {
				intersectInPreOrder(tree, root.getLeft(), newTree);
			} catch (IllegalArgumentException e) {

			}
		}

		if (root.getRigth() != null) {
			try {
				intersectInPreOrder(tree, root.getRigth(), newTree);
			} catch (IllegalArgumentException e) {

			}
		}
	}

	private AVLNode<T> updateBF(AVLNode<T> theRoot) {
		theRoot.updateHeight();
		if (theRoot.getBF() == -2) {
			if (theRoot.getLeft().getBF() <= 0) {// leave the smaller than
				theRoot = singleLeftRotation(theRoot);
			} else {
				theRoot = doubleLeftRotation(theRoot);
			}
		} else if (theRoot.getBF() == 2) {
			if (theRoot.getRigth().getBF() >= 0) { // leave the greater than
				theRoot = (singleRightRotation(theRoot));
			} else {
				theRoot = (doubleRightRotation(theRoot));
			}
		}

		return (theRoot);
	}

	private AVLNode<T> doubleRightRotation(AVLNode<T> theRoot) {
		AVLNode<T> newLeft = new AVLNode<T>(theRoot.getElement());		
		newLeft.setRigth(theRoot.getRigth().getLeft().getLeft());
		newLeft.setLeft(theRoot.getLeft());
		AVLNode<T> newRoot = theRoot.getRigth().getLeft();
		theRoot.getRigth().setLeft(newRoot.getRigth());
		newRoot.setLeft(newLeft);
		newRoot.setRigth(theRoot.getRigth());
		if (newRoot.getLeft() != null) {
			newRoot.getLeft().updateHeight();
		}

		if (newRoot.getRigth() != null) {
			newRoot.getRigth().updateHeight();
		}
		newRoot.updateHeight();

		return newRoot;
	}

	private AVLNode<T> singleRightRotation(AVLNode<T> theRoot) {
		AVLNode<T> newRoot = theRoot.getRigth();
		theRoot.setRigth(newRoot.getLeft());
		newRoot.setLeft(theRoot);
		if (newRoot.getLeft() != null) {
			newRoot.getLeft().updateHeight();
		}

		if (newRoot.getRigth() != null) {
			newRoot.getRigth().updateHeight();
		}
		newRoot.updateHeight();
		return newRoot;
	}

	private AVLNode<T> doubleLeftRotation(AVLNode<T> theRoot) { //SOLVE THIS
		AVLNode<T> newRight = new AVLNode<T>(theRoot.getElement());
		newRight.setLeft(theRoot.getLeft().getRigth().getRigth());
		newRight.setRigth(theRoot.getRigth());
		AVLNode<T> newRoot = theRoot.getLeft().getRigth();
		theRoot.getLeft().setRigth(newRoot.getLeft());
		newRoot.setRigth(newRight);
		newRoot.setLeft(theRoot.getLeft());
		if (newRoot.getLeft() != null) {
			newRoot.getLeft().updateHeight();
		}

		if (newRoot.getRigth() != null) {
			newRoot.getRigth().updateHeight();
		}
		newRoot.updateHeight();

		return newRoot;
	}

	private AVLNode<T> singleLeftRotation(AVLNode<T> theRoot) {
		AVLNode<T> newRoot = theRoot.getLeft();
		theRoot.setLeft(newRoot.getRigth());
		newRoot.setRigth(theRoot);

		if (newRoot.getLeft() != null) {
			newRoot.getLeft().updateHeight();
		}

		if (newRoot.getRigth() != null) {
			newRoot.getRigth().updateHeight();
		}
		newRoot.updateHeight();
		return newRoot;
	}

}
