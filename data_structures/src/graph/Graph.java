package graph;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

public class Graph<T> {

	protected static final int INDEX_NOT_FOUND = -1;
	protected static final double INFINITE = Double.MAX_VALUE;
	protected final static int EMPTY = -1;
	private ArrayList<GraphNode<T>> nodes;
	private boolean[][] edges;
	private double[][] weight;
	private int capacity;
	private double[] d;
	private int[] pd;
	private int[][] p;
	private double[][] a;

	public Graph(int i) {// i is the capacity of the graph. Is the maximum number of nodes that our graph
							// will deal with.
		this.setNodes(new ArrayList<GraphNode<T>>(i));
		this.setEdges(new boolean[i][i]);
		this.setWeight(new double[i][i]);
		this.capacity = i;
		this.d = new double[i];
		this.pd = new int[i];
		this.p = new int[i][i];
		this.a = new double[i][i];
	}

	protected ArrayList<GraphNode<T>> getNodes() {
		return nodes;
	}

	protected void setNodes(ArrayList<GraphNode<T>> nodes) {
		this.nodes = nodes;
	}

	protected boolean[][] getEdges() {
		return edges;
	}

	protected void setEdges(boolean[][] edges) {
		this.edges = edges;
	}

	protected double[][] getWeight() {
		return weight;
	}

	protected void setWeight(double[][] weight) {
		this.weight = weight;
	}

	/**
	 * Given a node's content, returns a non-negative integer representing the
	 * position on the graph.
	 * 
	 * @param element
	 *            content of the node to check.
	 * 
	 * @return a non-negative integer representing the node's coordinate if it
	 *         exists in the graph, and a negative one otherwise.
	 */
	public int getNode(T element) {
		GraphNode<T> nodeToCheck = new GraphNode<T>(element);
		int coordinate = 0;
		for (GraphNode<T> node : this.getNodes()) {
			if (node.equals(nodeToCheck)) {
				return coordinate;
			}
			coordinate++;
		}
		return INDEX_NOT_FOUND;

	}

	/**
	 * Get the number of nodes on the graph.
	 * 
	 * @return the number of nodes in the graph.
	 */
	public int getSize() {
		int size = this.nodes.size();
		return size;
	}

	/**
	 * Given an element, checks that there is no node containing it in the graph,
	 * and adds it with no edges.
	 * 
	 * @param Element
	 *            the element to be added to the graph
	 * @throws Exception
	 *             if there is already a node with that element in the graph, or
	 *             there is no space left for another node.
	 */
	public void addNode(T Element) throws Exception {
		int size = this.getSize();
		if (getNode(Element) == -1 && nodes.size() < edges[0].length) {
			this.getNodes().add(new GraphNode<T>(Element));
			for (int i = 0; i < size; i++) {
				edges[size][i] = false;
				edges[i][size] = false;
				weight[size][i] = 0;
				weight[i][size] = 0;

			}
		} else
			throw new Exception();
	}

	/**
	 * Checks whether there is an edge between the diven elements, by returning true
	 * if it exists, or false otherwise.
	 * 
	 * @param origin
	 *            the departure element.
	 * @param dest
	 *            the destination element.
	 * @return boolean true if there exists an edge, or false otherwise.
	 * @throws Exception
	 *             if the origin and destination given does not exist in the graph.
	 */

	public boolean existsEdge(T origin, T dest) throws Exception {
		int i = this.getNode(origin);
		int j = this.getNode(dest);
		boolean containsOrigin = nodes.contains(new GraphNode(origin));
		boolean containsDest = nodes.contains(new GraphNode(dest));
		if (!containsOrigin || !containsDest) {
			throw new Exception();
		}
		if (i == INDEX_NOT_FOUND || j == INDEX_NOT_FOUND) {
			return false;
		} else
			return edges[i][j];

	}

	/**
	 * Adds a new edge between the given origin and destination, with an specified
	 * weight.
	 * 
	 * @param origin
	 *            the departure element
	 * @param dest
	 *            the destination element
	 * @param weight
	 *            the desired weight of the new edge
	 * @throws Exception
	 *             if the edge already exists
	 */
	public void addEdge(T origin, T dest, double weight) throws Exception {
		if (!this.existsEdge(origin, dest)) {
			int i = this.getNode(origin);
			int j = this.getNode(dest);

			edges[i][j] = true;
			this.weight[i][j] = weight;
		} else

			throw new Exception("That edge already existed.");
	}

	public void resetVisited() {
		for (GraphNode<T> node : nodes) {
			node.setVisited(false);
		}
	}

	public String DFPrint(T origin) {
		this.resetVisited();
		return DFPrintRec(origin);
	}

	private String DFPrintRec(T origin) {

		nodes.get(getNode(origin)).setVisited(true);
		String path = origin + "-";
		int coord = getNode(origin);
		for (int i = 0; i < edges.length; i++) {
			if (edges[coord][i]) { // chance to jump
				if (!nodes.get(i).isVisited()) {
					path += DFPrintRec(nodes.get(i).getElement());
				}
			}
		}

		return path;
	}

	public String traverseGraphDF(T element) {
		for (GraphNode<T> node : nodes) {
			node.setVisited(false);
		}

		GraphNode<T> v = nodes.get(getNode(element));

		return DFPrint(v.getElement());
	}

	/**
	 * Removes the edge form the given origin to the given destination.
	 * 
	 * @param origin
	 *            int showing the departure node
	 * @param dest
	 *            int showing the arriving node
	 * @throws Exception
	 *             if the specified edge does not exist.
	 */
	public void removeEdge(T origin, T dest) throws Exception {
		if (existsEdge(origin, dest)) {
			int i = getNode(origin);
			int j = getNode(dest);
			edges[i][j] = false;
			weight[i][j] = 0;
		} else {
			throw new Exception("That edge does not exist.");
		}
	}

	/**
	 * Removes the specified node and the edges related to it.
	 * 
	 * @param element
	 *            contained in the node to be deleted.
	 */
	public void removeNode(T element) {
		int i = getNode(element);

		if (i >= 0) {

			int newSize = this.getSize() - 1;
			GraphNode<T> nodeToMove = nodes.get(newSize);
			nodes.remove(newSize);

			if (i != newSize) {
				nodes.set(i, nodeToMove);

				for (int j = 0; j < newSize; j++) {
					edges[j][i] = edges[j][newSize];
					edges[i][j] = edges[newSize][j];
					weight[i][j] = weight[newSize][j];
					weight[j][i] = weight[j][newSize];
				}

				edges[i][i] = edges[newSize][newSize];
				weight[i][i] = weight[newSize][newSize];
			} else {
				for (int j = 0; j < newSize; j++) {
					edges[i][j] = false;
					edges[j][i] = false;
					weight[i][j] = 0.0;
					weight[j][i] = 0.0;

				}
			}

		}
	}

	/**
	 * Prints in the screen the content of each node in the graph,
	 */
	public void print() {
		for (GraphNode<T> node : nodes) {
			node.print();
		}

		int size = this.getSize();

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.println(edges[i][j] + "(");
				System.out.println(weight[i][j] + ")");
			}
		}
	}

	/**
	 * Allows to access the D structure obtained as a result of the Dijkstra algorithm.
	 * 
	 * @return double[][] containing the D structure
	 */
	public double[][] getD() {
		double[][] aux = new double[1][d.length];
		aux[0] = d;
		return aux;
	}
	

	/**
	 * Allows to access the P structure of the Dijkstra algorithm.
	 * 
	 * @return int[] containing the P structure of the Dijkstra algorithm.
	 */
	public int[] getPD() {
		return pd;
	}

	/**
	 * Initializes the D and P structures for the Dijkstra algorithm .
	 * 
	 * @param element on which the initialization will be done.
	 */
	private void initDijkstra(T element) {
		int index = this.getNode(element);
		resetVisited();
		for (int i = 0; i < getSize(); i++) { // creating the S set by setting the visited flag to false

			if (weight[index][i] == 0 || i == index) {
				d[i] = INFINITE;
				pd[i] = INDEX_NOT_FOUND;
			} else {

				d[i] = weight[index][i];
				pd[i] = index;
			}
		}
		nodes.get(index).setVisited(true);

	}

	/**
	 * Performs the Dijkstra algorithm according to a given element.
	 * 
	 * @param element on which perform the algorithm.
	 */
	public void Dijkstra(T element) {
		this.initDijkstra(element);
		int counterSSet = 1;
		int size = this.getSize();
		while (counterSSet < size) {
			int w = selectW();
			nodes.get(w).setVisited(true);
			for (int i = 0; i < d.length; i++) {
				// if (!nodes.get(i).isVisited()) {
				if (d[w] + weight[w][i] < d[i] && edges[w][i]) {
					d[i] = d[w] + weight[w][i];
					pd[i] = w;
				}
				// }
			}
			counterSSet++;
		}
	}

	/**
	 * Selects the node to be the pivot for the new iteration of the Dijkstra algorithm.
	 * 
	 * @return int containing the index of the new pivot.
	 */
	private int selectW() {
		double comparator = INFINITE;
		int w = 0;
		for (int i = 0; i < d.length; i++) {
			if (!nodes.get(i).isVisited()) {
				if (d[i] < comparator) {
					w = i;
					comparator = d[i];
				}
			}
		}
		return w;
	}

	/**
	 * Represents the Dijkstra path in a string containing consecutevly the contents
	 * of the nodes in the path, starting from the departure and finishing in the
	 * arrival.
	 * 
	 * @param departure
	 *            T element contained in the departure node.
	 * @param arrival
	 *            T element contained in the arrival node.
	 * @return String containing the path between the two nodes, except if the
	 *         arrival and departure are the same. If the former case happens, the
	 *         empty string will be returned.
	 * @throws Exception
	 *             if one of the given nodes does not exist.
	 */
	public String printDijkstraPath(T departure, T arrival) throws Exception {
		if (this.getNode(arrival) < 0 || this.getNode(departure) < 0) {
			throw new Exception("One of the nodes does not exist.");
		}
		if (departure.equals(arrival)) {
			return "";
		}
		return printDijkstraPathRec(departure, arrival);
	}

	/**
	 * Recursive method to get the Dijkstra path between two nodes that are not the
	 * same.
	 * 
	 * @param departure
	 *            T element contained in the departure node.
	 * @param arrival
	 *            T element contained in the arrival node.
	 * @return String containing the path.
	 */
	private String printDijkstraPathRec(T departure, T arrival) {
		int arrivalIndex = this.getNode(arrival);
		if (!departure.equals(arrival)) {
			T newArrival = nodes.get(pd[arrivalIndex]).getElement();
			return this.printDijkstraPathRec(departure, newArrival) + arrival;
		} else {
			return departure.toString();
		}
	}

	/**
	 * Allows to access the cost matrix generated as a result of applying the Floyd
	 * algorithm over a graph. The content will vary depending on the last execution
	 * of the floyd() method.
	 * 
	 * @return double[][] containing the cost matrix of the Floyd algorithm.
	 */
	protected double[][] getA() {
		return a;
	}

	/**
	 * Allows to access the paths matrix resulting of the Floyd's algorithm. The
	 * content will vary depending on the last execution of the floyd() method.
	 * 
	 * @return int[][] containing the indexes to go from one node to another with
	 *         the minimum cost.
	 */
	protected int[][] getP() {
		return p;
	}

	/**
	 * Initializes all the properties needed for the execution of the Floyd's algorithm.
	 */
	protected void initsFloyd() {
		for (int i = 0; i < weight.length; i++) {
			for (int j = 0; j < weight[0].length; j++) {
				if (i == j) {
					a[i][j] = 0.0;
				} else {
					if (!edges[i][j]) {
						a[i][j] = INFINITE;

					} else {
						a[i][j] = weight[i][j];
					}
				}
				p[i][j] = EMPTY;
			}
		}
	}

	/**
	 * Performs the Floyd's algorithm over the graph, storing the results in A and P
	 * 
	 * @param An the number of iterations that we want the algorithm to perform.
	 */
	protected void floyd(int An) {
		initsFloyd();
		int size = getSize();
		for (int k = 0; k < An; k++) {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (a[i][k] + a[k][j] < a[i][j]) {
						a[i][j] = a[i][k] + a[k][j];
						p[i][j] = k;
					}
				}
			}
		}
	}

	/**
	 * Allows to get a string containing the minimum path between two nodes according to the Floyd's algorithm.
	 * 
	 * @param departure node
	 * @param arrival node
	 * @return String containing a representation of the Floyd's path
	 * @throws Exception
	 */
	public String printFloydPath(T departure, T arrival) throws Exception {
		int i = getNode(departure);
		int j = getNode(arrival);
		if(i<0 || j<0) {
			throw new Exception("One of the given nodes do not exist.");
		}
		int k = p[i][j];
		StringWriter path = new StringWriter();
		printFloydPathRec(departure, arrival, path);
		String finalPath = path.toString();
		try {
			path.close();
		} catch (IOException e) {

		}
		return finalPath;

	}

	/**
	 * Auxiliary recursive method for printing the floyd's path.
	 * 
	 * @param departure node
	 * @param arrival node
	 * @param path already found
	 * @return String containing the path
	 */
	private String printFloydPathRec(T departure, T arrival, StringWriter path) {
		int i = getNode(departure);
		int j = getNode(arrival);
		T aux = nodes.get(p[i][j]).getElement();
		if (getNode(aux) > 0) {
			printFloydPathRec(departure, aux, path);
			path.append(aux.toString());
			printFloydPathRec(aux, arrival, path);

		}

		return arrival.toString();
	}

	/**
	 * Prints Dijkstra on the screen.
	 */
	public void printDijkstra() {
		System.out.println("Dijkstra");

	}

}
