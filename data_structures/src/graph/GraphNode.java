package graph;

public class GraphNode<T> {

	private T element;
	private boolean visited;

	public GraphNode(T t) {
		this.setElement(t);
		this.setVisited(false);
	}

	public T getElement() {
		return element;
	}

	protected void setElement(T element) {
		this.element = element;
	}

	public boolean isVisited() {
		return visited;
	}

	protected void setVisited(boolean visited) {
		this.visited = visited;
	}

	public String toString() {

		return ("GN(N:" + this.getElement() + "/V:" + this.isVisited() + ")");

	}

	public void print() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraphNode other = (GraphNode) obj;
		if (element == null) {
			if (other.element != null)
				return false;
		} else if (!element.equals(other.element))
			return false;
		return true;
	}
	
	

}
