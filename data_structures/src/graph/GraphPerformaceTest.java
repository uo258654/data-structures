package graph;

import java.util.Random;
import algorithmics.TestBench;

public class GraphPerformaceTest {
	public static Graph<Integer> initGraph(long n) {
		int x = new Long(n).intValue();
		Graph<Integer> graph = new Graph<Integer>(x);
		try {
		
		for(int i = 1; i<=x; i++) {
			graph.addNode(i);
		}
		Random generator = new Random();
		
		for(int j =0; j<graph.getSize(); j++) {
			for (int k = 0;k<graph.getSize(); k++ ) {
				graph.addEdge(j, k, generator.nextInt(100));
			}			
		}
		
		return graph;
		}catch(Exception e) {
			
		}
		
		return graph;
	}
	
	public static void runDijkstra(long n)  {
		initGraph(new Long(n).intValue()).Dijkstra(new Long(n).intValue());
		TestBench.doNothing(n);
	}
	
	public static void runFloyd(long n)  {
		initGraph(new Long(n).intValue()).floyd(new Long(n).intValue());
		TestBench.doNothing(n);
	}

}
