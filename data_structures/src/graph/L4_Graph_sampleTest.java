package graph;

import static org.junit.Assert.*;

import org.junit.Test;

public class L4_Graph_sampleTest {

	@Test
	public void Test_Edit_A() {
		Graph<Character> g1 = new Graph<Character>(3);

		System.out.println("TEST EDIT A BEGINS ***");
		assertEquals(0, g1.getSize());

		try {
			g1.addNode('a');
		} catch (Exception e) {
			System.out.println("No repeated nodes are allowed" + e);
		}

		assertEquals(1, g1.getSize());
		assertEquals(0, g1.getNode('a'));
		assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, false }, { false, false, false } },
				g1.getEdges());
		assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } }, g1.getWeight());

		// Test nodes for nodes not found
		assertEquals(Graph.INDEX_NOT_FOUND, g1.getNode('b'));

		// No repeated nodes allowed
		try {
			g1.addNode('a');
		} catch (Exception e) {
			System.out.println("No repeated nodes are allowed" + e);
		}

		try {
			g1.addNode('b');
			g1.addNode('c');
		} catch (Exception e) {
			System.out.println("No repeated nodes are allowed" + e);
		}

		assertEquals(3, g1.getSize());
		assertEquals(0, g1.getNode('a'));
		assertEquals(1, g1.getNode('b'));
		assertEquals(2, g1.getNode('c'));

		assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, false }, { false, false, false } },
				g1.getEdges());
		assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } }, g1.getWeight());

		// Testing edges
		try {
			g1.existsEdge('b', 'd');
		} catch (Exception e) {
			System.out.println("Departure or arrival node does not exist" + e);
		}

		try {
			assertEquals(false, g1.existsEdge('b', 'c'));
		} catch (Exception e) {
			System.out.println("Departure or arrival node does not exist" + e);
		}

		assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, false }, { false, false, false } },
				g1.getEdges());
		assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } }, g1.getWeight());

		try {
			g1.addEdge('b', 'c', 5.0);
			assertEquals(true, g1.existsEdge('b', 'c'));
		} catch (Exception e) {
			System.out.println("Departure or arrival node does not exist" + e);
		}

		assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, true }, { false, false, false } },
				g1.getEdges());
		assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 5.0 }, { 0.0, 0.0, 0.0 } }, g1.getWeight());
		
		//Testing traverse		
		assertEquals("a-", g1.traverseGraphDF('a'));
		assertEquals("b-c-", g1.traverseGraphDF('b'));
		
		//Testing removal
		
		
			Graph<Character> g2 = g1;
			g2.removeNode('c');
			assertEquals(Graph.INDEX_NOT_FOUND, g2.getNode('c'));
			assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, false }, { false, false, false } },
					g2.getEdges());
			assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } }, g2.getWeight());
			
			g2 = new Graph<Character>(3);
			try {
				g2.addNode('a');
				g2.addNode('b');
				g2.addNode('c');
				g2.addEdge('b', 'c', 5.0);
			} catch (Exception e1) {
				fail();
			}
			
			
			try {
				g2.removeEdge('b', 'c');
				//assertFalse(g2.existsEdge('b', 'c'));
				assertArrayEquals(new boolean[][] { { false, false, false }, { false, false, false }, { false, false, false } },
						g2.getEdges());
				assertArrayEquals(new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } }, g2.getWeight());
			} catch (Exception e) {
				System.err.println("Those nodes does not exist.");
				fail();
				
			}
	}

}
