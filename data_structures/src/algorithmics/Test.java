package algorithmics;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void test() {
		assertEquals(720, Algorithmics.factorialRec(6));
	}

	@org.junit.Test
	public void test2() {
		assertEquals(720, Algorithmics.factorial(6));
	}

	@org.junit.Test
	public void testPow() {
		assertEquals(1099511627776L, Algorithmics.pow(40));
	}

}
