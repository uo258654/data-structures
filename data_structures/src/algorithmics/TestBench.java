package algorithmics;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;

public class TestBench {

	public static final int SLEEP_TIME = 25;

	public TestBench() {
		// this.test("linear.txt", 3, 1, 50, "algorithmics.Algorithmics", "linear");
		// this.test("cuadratic.txt", 3, 1, 50, "algorithmics.Algorithmics", "cuadratic");
		// this.test("cubic.txt", 3, 1, 20, "algorithmics.Algorithmics", "cubic");
		// this.test("logarithmic.txt", 3, 1, 50, "algorithmics.Algorithmics", "logarithmic");

		//this.test("powRec1.txt", 3, 1, 12, "algorithmics.Algorithmics", "powRec1");
		// this.test("powRec2.txt", 3, 1, 50, "algorithmics.Algorithmics", "powRec2");
		// this.test("powRec3.txt", 3, 1, 50, "algorithmics.Algorithmics", "powRec3");
		// this.test("powRec4.txt", 3, 1, 50, "algorithmics.Algorithmics", "powRec4");
		test("01_Graph_Floyd.txt", 3, 100, 300, "graph.GraphPerformaceTest", "runFloyd");
		test("02_Graph_Dijkstra.txt", 3, 100, 300, "graph.GraphPerformaceTest", "runDijkstra");
		test("03_Graph_Floyd.txt", 3, 100, 300, "graph.GraphPerformaceTest", "initGraph");

	}

	public static void main(String[] args) {
		new TestBench();
	}

	public static void testAlgorithm(String className, String methodName, long n) throws Exception {
		Class<?> myClass = null;

		// Loads the class dynamically using reflection.
		myClass = Class.forName(className);

		// Gets a method's instance.
		Class<?>[] params = new Class[1];
		params[0] = Long.TYPE;

		Method m = myClass.getMethod(methodName, params);

		// Calls the method in java using reflection.

		m.invoke(null, n);
	}

	/**
	 * Performs the test of the algorithm.
	 * 
	 * @param String
	 *            the name of the output file
	 * @param int
	 *            number of times to perform the experiment
	 * @param long
	 *            initial workload
	 * @param long
	 *            final workload
	 */
	public void test(String fileName, int samples, long startN, long endN, String className, String methodName) {

		FileWriter file = null;
		PrintWriter pw;

		try {
			file = new FileWriter(fileName);
			pw = new PrintWriter(file);

			for (long i = startN; i <= endN; i++) { // each sample between
													// the startN and endN
													// workloads
				long sum = 0;
				for (long j = 0; j < samples; j++) {
					long before = System.currentTimeMillis();
					testAlgorithm(className, methodName, i);
					;
					sum += (System.currentTimeMillis() - before); // add the
																	// result

				}
				pw.println(calculateAverage(sum, samples)); // calculate the
															// average and add
															// it to the file
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (file != null)
					file.close();
			} catch (Exception e2) {
			}
		}

	}

	public static void doNothing(long i) {
		System.out.println("interaction number: " + i);
		long endTime = System.currentTimeMillis() + SLEEP_TIME;
		while (System.currentTimeMillis() < endTime) {
			// do nothing
		}
	}

	public static long calculateAverage(long x, long y) {
		if (y == 0) {
			return 0;
		}
		return x / y;
	}

}
