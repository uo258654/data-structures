package algorithmics;

public class Algorithmics {

	/**
	 * Sample algorithm (with an O(n) complexity).
	 * 
	 * @param long
	 *            workload
	 */
	public static void linear(long n) {

		for (long i = 0; i < n; i++) {
			TestBench.doNothing(n); // n could be anynumber, it does not alter
									// anything in the behaviour of the
									// algorithm
		}

	}

	/**
	 * Sample algorithm (with an O(n²) complexity).
	 * 
	 * @param long
	 *            workload
	 */
	public static void cuadratic(long n) {
		for (long i = 0; i <= n; i++) {
			for (long j = 0; j <= n; j++) {
				TestBench.doNothing(n);
			}
		}
	}

	/**
	 * Sample algorithm (with an O(n³) complexity).
	 * 
	 * @param long
	 *            workload
	 */
	public static void cubic(long n) {
		for (long i = 0; i <= n; i++) {
			for (long j = 0; j <= n; j++) {
				for (long k = 0; k <= n; k++) {
					TestBench.doNothing(n);
				}
			}
		}
	}

	/**
	 * Sample algorithm (with an O(log2(n)) complexity).
	 * 
	 * @param long
	 *            workload
	 */
	public static void logarithmic(long n) {
		while (n > 0) {
			TestBench.doNothing(n);
			n = n / 2;
		}
	}

	/**
	 * Sample algorithm using iteration with an O(!n) complexity.
	 * 
	 * @param long
	 *            workload
	 */
	public static long factorial(long n) {
		long sum = 1;
		for (long i = n; i > 0; i--) {
			sum = sum * i;
		}

		return sum;
	}

	/**
	 * 
	 * 
	 * @param n
	 * @return
	 */
	public static long factorialRec(long n) {
		if (n == 0) {
			return 1;
		}

		return n * factorialRec(n - 1);
	}

	public static long pow(long n) {
		long result = 2;
		for (long i = 1; i < n; i++) {
			result = result * 2;
		}

		return result;
	}

	public static long powRec1(long n) {
		TestBench.doNothing(n);
		if (n == 0)
			return 1;

		return powRec1(n - 1) + powRec1(n - 1);
	}

	public static long powRec2(long n) {
		TestBench.doNothing(n);
		if (n == 0)
			return 1;

		return 2 * powRec2(n - 1);
	}

	public static long powRec3(long n) {
		TestBench.doNothing(n);
		if (n == 0)
			return 1;
		if (n % 2 != 0)
			return 2 * powRec3(n / 2) * powRec3(n / 2);
		return powRec3(n / 2) * powRec3(n / 2);
	}

	public static long powRec4(long n) {
		
		TestBench.doNothing(n);
		if (n == 0)
			return 1;
		else {
			long result = powRec4(n / 2);
			result *= result;
			if (n % 2 != 0)
				return 2 * result;
			return result;
		}
	}
}
